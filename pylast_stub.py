#!/usr/env python3

import os
import pylast
import pickle
from pprint import pprint
from threading import Thread
import time
import concurrent.futures
import configparser
from pylast import NetworkError, PlayedTrack, Track
from urllib.parse import quote_plus
import jmespath
from requests import get
from fuzzywuzzy import fuzz, process
from pylast import WSError
from retry import retry
from logging import getLogger
from functools import partial
from collections import defaultdict
from urllib.parse import parse_qs, urlparse

logger = getLogger(__name__)

LASTFM_BASE_URL = 'https://ws.audioscrobbler.com/2.0/' 


def safe_int(val):
    try:
        return int(val)
    except ValueError:
        return -1
    except TypeError:
        return 0


def build_url(base, method, api_key, json=False, **kwargs): 
    suffix = '&format=json' if json else '' 
    for key, value in kwargs.items(): 
        suffix += f'&{key}={quote_plus(value)}' 
    return f'{base}?method={method}&api_key={api_key}{suffix}' 


def get_track_playcount(track):
    try:
        return track.get_userplaycount()
    except pylast.WSError:
        logger.error('Error: Unable to find info for {} - {}'.format(track.artist, track.title))
        return -1
    except NetworkError:
        logger.error('Network Error, delaying 10 seconds')
        time.sleep(10)
        return get_track_playcount(track)


@retry(RuntimeError, tries=5, backoff=2, jitter=1, logger=logger)
def get_playcount_from_url(url):
    try:
        raw_result = get(url)
        result = raw_result.json()
        if 'error' in result:
            logger.warning("Couldn't process URL: %s", url)
            playcount = 'ERR'
            raise RuntimeError('URL process fail')
        else:
            playcount = safe_int(jmespath.search('track.userplaycount', result)) \
                or safe_int(jmespath.search('results.playcount', result))
        artist = jmespath.search('track.artist.name', result) \
            or parse_qs(urlparse(raw_result.url).query)['artist'][0]
        title = jmespath.search('track.name', result) \
            or parse_qs(urlparse(raw_result.url).query)['title'][0]
        return {f'{title} - {artist}': playcount}
    except Exception:
        logger.exception('uh oh')
        logger.info(f'raw_result: {raw_result}')
        logger.info(f'result: {result}')
        return {}


def get_playcounts_from_url_threaded(url_list, workers=None, verbose=False):
    user_playcount_dict = {}
    workers = workers or 64
    if verbose:
        start_time = time.time()
        print('timing enabled...')
    with concurrent.futures.ThreadPoolExecutor(max_workers=workers) as executor:
        for new_dict in executor.map(get_playcount_from_url, url_list):
            user_playcount_dict.update(new_dict)
    
    try:
        print(f'Took: {time.time() - start_time:.2f} seconds.')
    except NameError:
        pass

    return user_playcount_dict


def get_playcounts_threaded(track_list, workers=None, execute=None):
    user_playcount_dict = {}
    workers = workers or 4
    execute = execute or get_track_playcount
    start_time = time.time()
    with concurrent.futures.ThreadPoolExecutor(max_workers=workers) as executor:
        for track, count in zip(track_list, executor.map(execute, track_list)):
            user_playcount_dict['{} - {}'.format(track.title, track.artist)] = count
    print('Took: {}'.format(time.time() - start_time))
    return user_playcount_dict


def get_playcounts_single(track_list):
    user_playcount_dict = {}
    start_time = time.time()
    for track, count in zip(track_list, map(get_track_playcount, track_list)):
        user_playcount_dict[track] = count
    print('Took: {}'.format(time.time() - start_time))
    return user_playcount_dict


def get_playcounts_concurrent(track_list):
    user_playcount_dict = {}
    start_time = time.time()
    with concurrent.futures.ProcessPoolExecutor(max_workers=4) as executor:
        for track, count in zip(track_list, executor.map(get_track_playcount, track_list)):
            user_playcount_dict[track] = count
    print('Took: {}'.format(time.time() - start_time))
    return user_playcount_dict


# Ini
config = configparser.ConfigParser()
config.read('settings.ini')
env_vars = config['Environment Vars']

# PyLast
API_KEY = os.getenv('LASTFM_API_KEY') or env_vars['lastfm_api_key']
API_SECRET = os.getenv('LASTFM_API_SECRET') or env_vars['lastfm_api_secret']
lastfm_username = os.getenv('LASTFM_DEFAULT_USERNAME') or env_vars['lastfm_default_username']
password_hash = os.getenv('LASTFM_DEFAULT_PWHASH') or env_vars['lastfm_default_pwhash']
network = pylast.LastFMNetwork(api_key=API_KEY, api_secret=API_SECRET,
                               username=lastfm_username,
                               password_hash=password_hash)
# sets user to DEFAULT
user = pylast.User(lastfm_username, network)
api_key, *_ = network._get_ws_auth()


def get_all_tracks(user, chunk_size=1000, last_timestamp=0):
    playcount = user.get_playcount()
    tracks = set()
    last_timestamp = last_timestamp
    chunks = playcount // chunk_size + 1
    for i in range(chunks):
        print('Getting chunk {} of {}'.format(i + 1, chunks))
        recent_tracks = user.get_recent_tracks(limit=chunk_size, time_to=last_timestamp)
        tracks = tracks.union(recent_tracks)
        last_timestamp = int(recent_tracks[-1].timestamp)
    return sorted(list(tracks), key=lambda x: x.timestamp, reverse=True)


def create_library_set(track_history, user):
    library_set = set()
    for entry in track_history:
        library_set.add(entry.track)
    for track in library_set:
        track.username = user.name
    return library_set


def get_playlist_playcounts(playlist, network, username):
    pylast_tracks = [
        pylast.Track(
            artist=item.track.artist, 
            title=item.track.name, 
            network=network, 
            username=username
        ) for item in playlist.items
    ]
    return get_playcounts_threaded(pylast_tracks, 64)


def build_getinfo_url(item, base_url=None, username=None, *args, **kwargs):
    api_key, *_ = network._get_ws_auth()
    base_url = base_url or LASTFM_BASE_URL
    username = username or lastfm_username
    url = build_url(
        base=base_url,
        method='track.getInfo',
        api_key=api_key,
        json=True,
        **dict(
            artist=item.track.artist,
            track=item.track.name,
            username=username
        )
    )
    return url


def get_lastfm_urls_from_playlist(playlist, *args, **kwargs):

    pylast_track_urls = [
        build_getinfo_url(item, *args, **kwargs) for item in playlist.items
    ]

    return pylast_track_urls


@retry((WSError, RuntimeError), tries=3, delay=2, logger=logger)
def get_pylast_recent_tracks(username, limit=50, page=1):
    user = pylast.User(username, network)
    url = build_url(
        LASTFM_BASE_URL, 
        'user.getrecenttracks', 
        api_key, 
        json=True, 
        user=user.name,
        limit=str(limit),
        page=str(page)
    )
    response = get(url)
    try:
        tracks = response.json()['recenttracks']['track'][1:]
    except KeyError:
        raise RuntimeError(f'Failed to parse results for {url} from: {response} | {response.json()}')
    pylast_tracks = [
        PlayedTrack(
            Track(
                track['artist']['#text'],
                track['name'],
                network
            ),
            track['album']['#text'],
            track['date']['#text'],
            track['date']['uts']
        ) 
        for track in tracks
    ]
    return pylast_tracks


def get_all_tracks_concurrently(username, limit=1000, workers=16):
    recents_url = f'https://ws.audioscrobbler.com/2.0/?' \
                  f'method=user.getrecenttracks&user={username}' \
                  f'&api_key={api_key}' \
                  f'&format=json&limit={limit}'
    result = get(recents_url).json()
    total_pages = int(result['recenttracks']['@attr']['totalPages'])
    tracks = set()
    get_recent_tracks = partial(get_pylast_recent_tracks, username, limit)
    with concurrent.futures.ThreadPoolExecutor(max_workers=workers) as executor:
        for recent_tracks in executor.map(
                get_recent_tracks, range(1, total_pages + 1)
        ):
            tracks = tracks.union(recent_tracks)

    return tracks

def sort_all_tracks(tracks):
    return sorted(list(tracks), key=lambda x: int(x.timestamp), reverse=True)


def create_library_dict(track_history, user):
    library_dict = defaultdict(int)
    for entry in track_history:
        library_dict[entry.track] += 1
    return library_dict


def simplify_library_dict(lib_dict):
    simple_lib_dict = {
        f'{track.title} - {track.artist}': count 
        for track, count in lib_dict.items()
    }
    return simple_lib_dict


def search_dict(dict_, needle, fuzz_ratio=70, filter_method=fuzz.token_sort_ratio):
    if not isinstance(fuzz_ratio, int):
        # Force to integer
        fuzz_ratio = 70
    return list(
        filter(
            lambda x: filter_method(needle, x) > fuzz_ratio,
            dict_.keys()
        )
    )


def get_library_playcounts(playlist, library_dict, fuzz_ratio=None):
    playcounts = {}
    for item in playlist.items:
        key = f'{item.track.name} - {item.track.artist}'
        count = library_dict.get(key)
        if not count and fuzz_ratio:
            fuzzy_matches = process.extractOne(library_dict, key)
            if len(fuzzy_matches) == 1:
                key = fuzzy_matches[0]
                count = library_dict.get(key)
        playcounts[key] = count
    return playcounts


def generate_lastfm_dict(username):
    track_history = get_all_tracks_concurrently(username)
    all_tracks = sort_all_tracks(track_history)
    library_dict = create_library_dict(all_tracks, username)
    simple_lib_dict = simplify_library_dict(library_dict)
    return simple_lib_dict


if __name__ == "__main__":

    with open('wisdomwolf_tracks.p', 'rb') as f:
        wisdomwolf_tracks = pickle.load(f)

    wisdomwolf_library = create_library_set(wisdomwolf_tracks, user)
    print('To get playcounts run: get_playcounts_threaded(wisdomwolf_library, 16/32/64)')
    
