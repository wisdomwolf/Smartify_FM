import configparser
import logging
import re
from urllib import parse as urlparse

import pylast
import spotiwise
from apscheduler.schedulers import SchedulerNotRunningError
from apscheduler.schedulers.background import BackgroundScheduler
from aws_requests_auth import boto_utils
from aws_requests_auth.aws_auth import AWSRequestsAuth
#from spotiwise_classes import SpotipyArtist, SpotipyAlbum, SpotipyTrack, SpotipyPlayback, SpotipyPlaylist, Scrobbler
from spotiwise import oauth2, util
from spotiwise.object_classes import (SpotiwiseAlbum, SpotiwiseArtist,
                                      SpotiwiseItem, SpotiwisePlayback,
                                      SpotiwisePlaylist, SpotiwiseTrack)

from pylast_stub import (
    get_playlist_playcounts, 
    get_lastfm_urls_from_playlist,
    get_playcounts_from_url_threaded
)
from spotiwise_testing_base import (
    sp, LASTFM_API_KEY, LASTFM_API_SECRET, password_hash
)

#redirect_url = 'http://127.0.0.1:5000/login/authorized'
#scopes = ' '.join(SHARIFY_SCOPES)


scheduler = BackgroundScheduler()
username = 'wisdomwolf'

def get_aws_auth(url):
    api_gateway_netloc = urlparse.urlparse(url).netloc
    api_gateway_region = re.match(
        r"[a-z0-9]+\.execute-api\.(.+)\.amazonaws\.com",
        api_gateway_netloc
    ).group(1)

    return AWSRequestsAuth(
        aws_host=api_gateway_netloc,
        aws_region=api_gateway_region,
        aws_service='execute-api',
        **boto_utils.get_credentials()
    )

# auth_manager = spotiwise.oauth2.SpotifyOAuth(
#     scope=scopes,
#     client_id=SPOTIFY_APP_ID,
#     client_secret=SPOTIFY_APP_SECRET,
#     redirect_uri=redirect_url,
#     username=username
# )
# sp = spotiwise.Spotify(auth_manager=auth_manager)

# oauth = util.prompt_for_user_token(
#     username=username,
#     scope=scopes,
#     client_id=SPOTIFY_APP_ID,
#     redirect_uri=redirect_url,
#     client_secret=SPOTIFY_APP_SECRET
# )
# sp = spotiwise.Spotify(oauth=oauth)


config = configparser.ConfigParser()
config.read('settings.ini')
env_vars = config['Environment Vars']
password_hash = env_vars['lastfm_default_pwhash']
network = pylast.LastFMNetwork(api_key=LASTFM_API_KEY, api_secret=LASTFM_API_SECRET, username='wisdomwolf', password_hash=password_hash)
user = pylast.User('wisdomwolf', network)


def restart_scheduler():
    global scheduler
    try:
        scheduler.shutdown()
    except SchedulerNotRunningError:
        logging.info('Scheduler already shutdown')
        pass
    scheduler = BackgroundScheduler()
    job = scheduler.add_job(my_scrobbler.update_track, 'interval', seconds=10, replace_existing=True)
    scheduler.start()
    logging.info('Scheduler Restarted')
    print('Scheduler restarted successfully')
    
    
def log_filter(record):
    if record.levelno < 30 and 'apscheduler' in record.name:
        return False
    else:
        return True


logger = logging.getLogger(__name__)

def get_playlist_uri(playlist_name):
    playlists = sp.current_user_playlists()
    playlist_name_uri_map = {playlist.name: playlist.uri for playlist in playlists}
    return playlist_name_uri_map.get(playlist_name)


def get_playlist_json(playlist_name):
    user, obj, uid = get_playlist_uri(playlist_name).split(':')
    return sp._user_playlist(user, uid)


def get_playlist(playlist_name):
    playlist_uri = get_playlist_uri(playlist_name).split(':')
    return sp.user_playlist(playlist_uri[2], playlist_uri[-1], precache=True)


def create_spotiwise_playlist(playlist_name):
    return SpotiwisePlaylist(**get_playlist_json(playlist_name), sp=sp, precache=True)


def get_undiscovered_tracks(playlist):
    playcounts = get_playlist_playcounts(playlist)
    undiscovered = [track for track, count in playcounts.items() if count < 1]
    return [item.track for item in playlist.tracks if '{} - {}'.format(item.track.name, item.track.artist) in undiscovered]


def get_track_uris(track_list):
    return [item.track.uri for item in track_list]


def load_all_playlists(batch_size=50):
    results = sp.current_user_playlists()
    playlists = results.copy()
    offset = 50
    while len(results) > 0:
        offset += batch_size
        results = sp.current_user_playlists(batch_size, offset)
        playlists.extend(results)
    return playlists


def get_playcounts(playlist, *args, **kwargs):
    """
    Retrieves play counts for supplied playlist and returns them as dict
    :param playlist: playlist to get playcounts for
    :param args:
    :param kwargs:
    :return: dictionary with song as key and lastfm as value
    :rtype: dict
    """
    if not playlist.items:
        playlist.load_tracks()
    urls = get_lastfm_urls_from_playlist(playlist, *args, **kwargs)
    playcounts = get_playcounts_from_url_threaded(urls)
    return playcounts


def get_track_ids_from_playlist_ids(playlist_ids):
    tracks = set()
    for playlist_id in playlist_ids:
        playlist = sp.user_playlist('spotify', playlist_id, precache=True)
        tracks.update(set(track.id for track in playlist.tracks))
    return tracks


def sync_playlists(sync_config):
    playlists = sync_config.get('playlists')
    sync_map = sync_config.get('sync_map', {})
    for target_key, key_list in sync_map.items():
        target_id = playlists.get(target_key)
        playlist_ids = [playlists.get(key) for key in key_list]
        track_ids = get_track_ids_from_playlist_ids(playlist_ids)
        # track_ids = [track.id for track in tracks]
        sp.user_playlist_replace_tracks('wisdomwolf', target_id, track_ids)
        logger.info(f'Sync completed for {target_key}')


def archive_playlists(sync_config):
    playlists = sync_config.get('playlists')
    archive_map = sync_config.get('archive_map', {})
    for source_key, destination_key in archive_map.items():
        source_id = playlists.get(source_key)
        destination_id = playlists.get(destination_key)
        archive_tracks(source_id, destination_id)
        logger.info(f'Successfully archived {source_key}')


def archive_tracks(source_playlist_id, destination_list_id):
    existing_tracks = get_track_ids_from_playlist_ids([destination_list_id])
    potential_new_tracks = get_track_ids_from_playlist_ids([source_playlist_id])
    new_track_ids = potential_new_tracks.difference(existing_tracks)
    if new_track_ids:
        current_user_id = sp.me()['id']
        sp.user_playlist_add_tracks(
            current_user_id, destination_list_id, new_track_ids
        )


if __name__ == '__main__':
    my_scrobbler = Scrobbler(sp, user, network)
    my_scrobbler.scheduler.add_job(my_scrobbler.update_track, 'interval', seconds=10)
    logging.basicConfig(
        format='%(asctime)s:%(levelname)s:%(name)s - %(message)s',
        filename='smartify_testing.log',
        level=logging.INFO
    )
    logger.handlers[0].addFilter(log_filter)
