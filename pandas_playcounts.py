import pandas as pd
import requests
from spotiwise.object_classes import SpotiwisePlaylist, SpotiwiseTrack

from smartify_daemon import get_playlist, get_playcounts

LASTFM_USERS = ['wisdomwolf', 'PurpleLover5198', 'raiaku']
FRESH_TRACKS = get_playlist('Fresh Tracks')

def get_lf_playcount(**kwargs):
    """Gets last.fm user playcount info for a track

    :return: playcount
    :rtype: int
    """
    url = 'https://functions.wiseapp.xyz/function/lastfm-playcount.openfaas-fn'
    response = requests.get(url, params=kwargs)
    if response:
        return safe_int(response.json().get('results', {}).get('playcount', 0))
    else:
        return 'ERR'


def build_playcounts_df(playlist: SpotiwisePlaylist, users: list):
    """Generate pandas dataframe to visualize playcounts for multiple users

    :param playlist: The playlist to generate dataframe from
    :type playlist: SpotiwisePlaylist
    :param users: list of users to get playcounts for
    :type users: list
    :return: dataframe
    :rtype: pd.DataFrame
    """
    df = pd.DataFrame(columns=['track']).set_index('track') 
    for user in users:
        results = get_playcounts(playlist, username=user)
        user_df = pd.DataFrame(list(results.items()), columns=['track', user])
        user_df = user_df.set_index('track')
        df = user_df.join(df, how='outer')
    return df


def add_track_to_results(df, track, users):
    """Create single dataframe row with track and playcounts per user

    :param df: The dataframe to append results to
    :type df: pd.DataFrame
    :param track: The track to use for getting playcounts
    :type track: SpotiwiseTrack
    :param users: list of users to get playcounts for
    :type users: list
    :return: Dataframe with results appended
    :rtype: pd.DataFrame
    """
    vals = {'Track': f'{track.name} - {track.artist}'}
    for user in users:
        result = get_lf_playcount(username=user, artist=track.artist, title=track.name)
        vals[user] = result
    return df.append(vals, ignore_index=True)


def safe_int(val):
    try:
        return int(val)
    except ValueError:
        return -1
    except TypeError:
        return None


def unheard_tracks(df: pd.DataFrame, user: str):
    """Get dataframe of tracks that a given user hasn't listened to

    :param df: Playlist DataFrame
    :type df: pd.DataFrame
    :param user: The user to filter DataFrame with
    :type user: str
    :return: Dataframe of unheard tracks
    :rtype: pd.DataFrame
    """
    return df[df[user] == 0]


def main():
    playcounts_df = build_playcounts_df(FRESH_TRACKS, LASTFM_USERS)
    print('playcounts dataframe created as `playcounts_df`')


if __name__ == '__main__':
    main()

